import { Component, Input, OnInit } from '@angular/core';

export interface SubMenuItem {
  title: string;
  link: string;
}

export interface MenuItem {
  title: string;
  sub: SubMenuItem[];
}

@Component({
  selector: 'app-menu-component',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
})

export class MenuComponent implements OnInit {

  @Input() menuContent: MenuItem[];
  ngOnInit() {
  }
}
