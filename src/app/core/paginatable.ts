export interface Paginatable {
  skip?: number;
  top?: number;
}

const DEFAULT_SKIP = 0;
const DEFAULT_TOP = 10;

export function fixPagination<T extends Paginatable>(obj: T): T {
  if (!obj.skip) {
    obj.skip = DEFAULT_SKIP;
  }
  if (!obj.top) {
    obj.top = DEFAULT_TOP;
  }
  return obj;
}
