export const UNIVERSITIES: any[] = [
  {
    name: 'БГУ',
    faculties: ['ФПМИ', 'Биологический', 'Химический', 'РАФ'],
  },
  {
    name: 'БНТУ',
    faculties: ['Тракторный', 'инженерный', 'ПГС'],
  },
  {
    name: 'БГУИР',
    faculties: ['ФИТР', 'КСИС', 'ФКП'],
  },
];

export const PRIMARY_SKILLS: string[] = [
  'Java',
  'C++',
  'Assembler',
  'Multithreading',
  'Math',
  'JavaScript',
  'golang',
  'C',
  'Physics',
];
