import { User } from './user';

export interface Student extends User {
  university: string;
  faculty: string;
  primarySkill: string;
  rate: number;
  graduationYear: number;
}
