import { User } from './user';

export interface Teacher extends User {
  primarySkill: string;
  university: string;
}
