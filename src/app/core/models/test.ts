import { User } from './user';

export declare type AnyQuestion = OneAnswerQuestion | MultipleAnswersQuestion | InputAnswerQuestion | Question;

export enum QuestionType {
  OneAns = 'oneAnswer',
  MultipleAns = 'manyAnswers',
  InputAns = 'inputAns',
  TextareaAns = 'textAns',
}

export interface Question {
  id: string;
  tags: string[];
  training: boolean;
  isAvailable: boolean;
  complained: boolean;
  type: QuestionType;
  questionText: string;
  weight: number;
  attempts: number;
  successfulAttempts: number;
  author: User;
  isFilled: boolean;
  userAnswer: string;
  // [_: string]: any;
}

export interface OneAnswerQuestion extends Question {
  answers: {id: string, text: string}[];
  rightAnswer: string;
}

export interface MultipleAnswersQuestion extends Question {
  answers: {id: string, text: string}[];
  rightAnswers: string[];
}

export interface InputAnswerQuestion extends Question {
  rightAnswer: string;
}
