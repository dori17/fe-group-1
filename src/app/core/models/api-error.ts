export interface ApiError {
  code: number;
  description: string;
  data?: {
    field: string,
    description: string,
  }[];
}
