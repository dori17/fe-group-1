export interface User {
  id?: string;
  email: string;
  name: string;
  surname: string;
  role: UserRole;
  [_: string]: any;
}

export enum UserRole {
  STUDENT = 'student',
  TEACHER = 'teacher',
  ADMIN = 'admin',
}
