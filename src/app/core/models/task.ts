export interface TaskAssignment {
  assignerId: string;
  assignerName: string;
  assignerSurname: string;
  assigneeId: string;
  assigneeName: string;
  assigneeSurname: string;
  deadline?: Date;
}

export interface Task {
  id: string;
  name: string;
  tags: string[];
  weight: number;
  isAvailable: boolean;
  assignments?: TaskAssignment[];
  description: string;
}

export interface NewTaskConfig {
  name: string;
  tags: string[];
  weight: number;
  description: string;
}

export interface UpdateTaskFields {
  name?: string;
  tags?: string[];
  weight?: number;
  description?: string;
  isAvailable?: boolean;
}
