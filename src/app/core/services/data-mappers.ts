import { User, UserRole, TaskAssignment, Task } from '../models';

export function backendDateToDate(beDate: number): Date {
  return new Date(beDate);
}

export function dateToBackendDate(date: Date): number {
  return date.valueOf();
}

export function roleToBackendRole(role: UserRole): string {
  switch (role) {
    case UserRole.ADMIN:
      return 'ROLE_ADMIN';
    case UserRole.STUDENT:
      return 'ROLE_STUDENT';
    case UserRole.TEACHER:
      return 'ROLE_TEACHER';
  }
}

export function userToBackendUser(user: User): Object {
  const dump: any = <Object>Object.assign({}, user);
  dump.role = roleToBackendRole(user.role);
  dump.graduation = user.graduationYear;
  delete dump.graduationYear;
  return dump;
}

export function backendRoleToRole(backendRole: string): UserRole {
  switch (backendRole) {
    case 'ROLE_ADMIN':
      return UserRole.ADMIN;
    case 'ROLE_STUDENT':
      return UserRole.STUDENT;
    case 'ROLE_TEACHER':
      return UserRole.TEACHER;
    default:
      throw new Error(`Unknown role ${backendRole}`);
  }
}

export function backendUserToUser(backendUser: any): User {
  const user: User = <User>Object.assign({}, backendUser);
  user.role = backendRoleToRole(backendUser.role);
  if (user.graduation) {
    delete user.graduation;
    user.graduationYear = backendUser.graduation;
  }
  return user;
}

export function backendTaskAssignmentToClient(beTA: any): TaskAssignment {
  const clientTA: TaskAssignment = <TaskAssignment>Object.assign({}, beTA);
  if (beTA.deadline) {
    clientTA.deadline = backendDateToDate(beTA.deadline);
    console.log(clientTA.deadline);
  }
  return clientTA;
}

export function backendTaskToTask(beTask: {
  task: any,
  assignments?: any[],
}): Task {
  // const clientTask = Object.assign({}, beTask);
  // delete clientTask.users;
  // return <Task>clientTask;
  const clientTask = Object.assign({}, beTask.task);
  delete clientTask.users;
  if (beTask.assignments) {
    clientTask.assignments = beTask.assignments.map(backendTaskAssignmentToClient);
  }
  return <Task>clientTask;
}
