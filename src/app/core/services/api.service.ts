import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
import { ApiError } from '..';

const API_URL = environment.apiUrl;
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*',
  }),
};

@Injectable()
export class ApiService {
  private token: string;
  private refreshToken: string;

  constructor(private http: HttpClient) { }

  private formatErrors(error: HttpErrorResponse) {
    console.log('in format error');
    return throwError(<ApiError>error.error);
  }

  setToken(token: string) {
    this.token = token;
    httpOptions.headers = httpOptions.headers.set(
      'Authorization',
      token ? `Bearer ${this.token}` : '',
    );
  }

  getToken(): string {
    return this.token;
  }

  setRefreshToken(refreshToken: string) {
    this.refreshToken = refreshToken;
  }

  getRefreshToken(): string {
    return this.refreshToken;
  }

  get<T>(request: string, params: Object = {}): Observable<T> {
    let requestParams:string = '/?';
    Object.keys(params).forEach((key) => {
      if (Array.isArray(params[key])) {
        params[key].forEach((value) => {
          requestParams += `${key}=${value}&`;
        });
      } else {
        requestParams += `${key}=${params[key]}&`;
      }
    });
    requestParams = requestParams.slice(0, -1);
    console.log(requestParams);
    return this.http.get<T>(
      `${API_URL}${request}${requestParams}`,
      {
        headers: httpOptions.headers,
      },
    ).pipe(catchError(this.formatErrors));
  }

  put<T>(request: string, body: any = {}): Observable<T> {
    return this.http.put<T>(
      `${API_URL}}${request}`,
      JSON.stringify(body),
      httpOptions,
    )
      .pipe(catchError(this.formatErrors));
  }
  post<T>(request: string, body: any = {}): Observable<T> {
    return this.http.post<T>(
      `${API_URL}${request}`,
      JSON.stringify(body),
      httpOptions,
    ).pipe(catchError(this.formatErrors));
  }

  delete<T>(request): Observable<T> {
    return this.http.delete<T>(
      `${API_URL}${request}`,
      {
        headers: httpOptions.headers,
      },
    ).pipe(catchError(this.formatErrors));
  }
}
