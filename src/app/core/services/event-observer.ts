import { Injectable } from '@angular/core';

declare type Listener = (any) => void;

@Injectable()
export class EventObserver {
  events: {
    [eventName: string]: Listener[],
  } = {};

  addListener(eventName: string, listener: Listener) {
    if (!this.events[eventName]) {
      this.events[eventName] = [];
    }
    this.events[eventName] = [];
  }

  emit(eventName: string, data?: any) {
    if (this.events[eventName]) {
      this.events[eventName].forEach(cb => cb(data));
    }
  }

  removeListener(eventName: string, listener: Listener) {
    const ind = this.events[eventName].indexOf(listener);
    this.events[eventName].splice(ind);
  }

  constructor() {
  }
}
