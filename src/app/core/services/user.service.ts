import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';

import { ApiService } from './api.service';
import { User } from '../models/user';
// import { Student } from '../models/student';
// import { Teacher } from '../models/teacher';
import { AuthService } from './auth.service';
import { Paginatable, fixPagination } from '../paginatable';
import { userToBackendUser, backendUserToUser } from './data-mappers';

const API_METHOD: string = '/users';

interface LocalStorageSession {
  token: string;
  refreshToken: string;
  user: User;
}

@Injectable()
export class UserService {
  private user: User = null;

  constructor(
    private apiService: ApiService,
    private authService: AuthService,
  ) {
    if (localStorage.getItem('tesexa')) {
      const session: LocalStorageSession =
        JSON.parse(localStorage.getItem('tesexa'));
      this.user = session.user;
      this.apiService.setToken(session.token);
      this.apiService.setRefreshToken(session.refreshToken);
    }
  }

  getUser(): User {
    return this.user;
  }

  isAuthorized(): boolean {
    return !!this.user;
  }

  logIn(email: string, password: string): Observable<User> {
    return this.authService.auth(email, password)
      .pipe(
        tap((user) => {
          this.user = user;
          localStorage.setItem(
            'tesexa',
            JSON.stringify({
              user,
              token: this.apiService.getToken(),
              refreshToken: this.apiService.getRefreshToken(),
            }),
          );
        }),
    );
  }

  logOut(): Observable<any> {
    return this.apiService.get('/logout')
      .pipe(
        tap(() => {
          this.user = null;
          this.apiService.setToken(null);
          this.apiService.setRefreshToken(null);
          localStorage.removeItem('tesexa');
        }),
    );
  }

  register(user: User): Observable<User> {
    return this.apiService.post(
      '/registration',
      userToBackendUser(user),
    ).pipe(
      map(beUser => backendUserToUser(beUser)),
    );
  }

  deleteUser(id: string): Observable<any> {
    return this.apiService.delete(`${API_METHOD}/${id}`);
  }

  getUserById(id: string): Observable<User> {
    return this.apiService.get(`${API_METHOD}/${id}`)
      .pipe(
        map(beUser => backendUserToUser(beUser)),
    );
  }

  getUsers(options: Paginatable = {}): Observable<User[]> {
    return this.apiService.get<any[]>(
      '/getAllUsers',
      fixPagination(options),
    )
      .pipe(
        map(beUsers =>
          beUsers.map(backendUserToUser),
        ),
    );
  }

  getStudents(options: Paginatable = {}): Observable<User[]> {
    return this.getUsers(options);
  }

  // private getAllUsersByRole<T extends User>(role: UserRole): Observable<T[]> {
  //   return this.getUsers()
  //     .pipe(map(users => <T[]>users.filter(user => user.role === role)));
  // }

  // getAllStudents(): Observable<Student[]> {
  //   return this.getAllUsersByRole<Student>(UserRole.STUDENT);
  // }

  // getAllTeachers(): Observable<Teacher[]> {
  //   return this.getAllUsersByRole<Teacher>(UserRole.TEACHER);
  // }
}
