import { Injectable } from '@angular/core';
import { Observable/*, forkJoin*/ } from 'rxjs';
import { map/*, mergeMap*/ } from 'rxjs/operators';

import { ApiService } from './api.service';
import { Task, NewTaskConfig, TaskAssignment } from '../models/task';
import { fixPagination } from '../paginatable';
import { backendTaskToTask, backendTaskAssignmentToClient, dateToBackendDate } from './data-mappers';
import { UserService } from './user.service';

const API_METHOD: string = '/tasks';

@Injectable()
export class TaskService {

  constructor(
    private apiService: ApiService,
    private userService: UserService,
  ) { }

  // private mapTaskAssignment(task: Task): Observable<Task> {
  //   return this.getTaskAssignment(task.id)
  //     .pipe(
  //       map((ta) => {
  //         task.assignment = ta;
  //         return task;
  //       }),
  //   );
  // }

  // private mockMapTaskAssignment = (task: Task) => {
  //   task.assignment = {
  //     assignerId: '1e232d28-9d0a-4b93-84cb-1bcab728aee7',
  //     assigneeId: '077640d8-455c-4653-9bf3-d961d99cf14a',
  //     deadline: new Date(),
  //   };
  //   return task;
  // }

  getTasks(params: {
    top?: number,
    skip?: number,
    userId?: string,
    taskName?: string,
    tags?: string[],
    mark?: number,
  } = {}): Observable<Task[]> {
    // const mapper = task => this.mapTaskAssignment(task);
    // const mockMapper = this.mockMapTaskAssignment;
    return this.apiService.get<any[]>(
      `${API_METHOD}`,
      fixPagination(params),
    ).pipe(
      map(beTasks => beTasks || []),
      map(beTasks => beTasks.map(backendTaskToTask)),
    );
  }

  getTaskById(id: string): Observable<Task> {
    // const mapper = task => this.mapTaskAssignment(task);
    return this.apiService.get(
      `${API_METHOD}/${id}`,
    ).pipe(
      map(beTask => backendTaskToTask(<{task: any, assignments?: any[]}>beTask)),
      // map(mergeMap(task => (mapper(task)))),
      // map(task => this.mockMapTaskAssignment(task)),
    );
  }

  createTask(taskConfig: NewTaskConfig): Observable<Task> {
    return this.apiService.post<any>(
      `${API_METHOD}`,
      taskConfig,
    ).pipe(
      map(beTask => backendTaskToTask(beTask)),
    );
  }

  getTaskAssignment(
    taskId: string,
    userId: string = this.userService.getUser().id,
  ): Observable<TaskAssignment> {
    return this.apiService.get<any>(
      `${API_METHOD}/${taskId}/assignments/${userId}`,
    ).pipe(
      map(beTA => backendTaskAssignmentToClient(beTA)),
    );
  }

  assignTask(userId: string, taskId: string, deadline?: Date): Observable<TaskAssignment> {
    return this.apiService.put<any>(
      `${API_METHOD}/${taskId}/assignments/${userId}`,
      deadline ? dateToBackendDate(deadline) : undefined,
    ).pipe(
      map(backendTaskAssignmentToClient),
    );
  }

  updateTask(editedTask: Task): Observable<Task> {
    return this.apiService.put<Task>(
      `${API_METHOD}/${editedTask.id}`,
      editedTask,
    );
  }
}
