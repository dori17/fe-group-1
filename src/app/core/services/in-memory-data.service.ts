import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api';
import { USERS } from '../mocks/users';
import { TASKS } from '../mocks/tasks';
import { UNIVERSITIES, PRIMARY_SKILLS } from '../mocks/static';
import { QUESTIONS } from '../mocks/tests';

@Injectable()
export class InMemoryDataService implements InMemoryDbService {

  createDb() {
    return {
      users: USERS,
      tasks: TASKS,
      primarySkills: PRIMARY_SKILLS,
      universities: UNIVERSITIES,
      questions: QUESTIONS,
    };
  }

  constructor() { }
}
