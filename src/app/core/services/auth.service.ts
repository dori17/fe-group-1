import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { map } from 'rxjs/operators';
import { User } from '../models';
import { backendUserToUser } from './data-mappers';

export interface AuthResponse {
  access_token: string;
  refresh_token: string;
  user: any;
  date: any;
}

@Injectable()
export class AuthService {
  constructor(
    private httpClient: HttpClient,
    private apiService: ApiService) { }

  auth(email: string, password: string): Observable<User> {
    return this.httpClient.post<AuthResponse>(
      `${environment.authUrl}/token`,
      {
        password,
        email,
      },
      {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'Access-Control-Allow-Origin': '*',
        }),
      }).pipe(
        map((response) => {
          this.apiService.setToken(response.access_token);
          this.apiService.setRefreshToken(response.refresh_token);
          return backendUserToUser(response.user);
        }),
      );
  }
}
