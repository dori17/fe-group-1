import { Injectable } from '@angular/core';

import { AnyQuestion } from '../models/test';
import { Observable, of } from 'rxjs';
import { QUESTIONS } from '../mocks/tests';

@Injectable()
export class TestService {
  constructor() { }

  getFullTest() : Observable<AnyQuestion[]> {
    return of(QUESTIONS);
  }
}
