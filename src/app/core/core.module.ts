import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';

import { ApiService } from './services/api.service';
import { AuthService } from './services/auth.service';
import { UserService, TaskService, TestService } from './services';
import { EventObserver } from './services/event-observer';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
  ],
  providers: [
    AuthService,
    ApiService,
    UserService,
    TaskService,
    TestService,
    EventObserver,
  ],
  declarations: [],
})
export class CoreModule { }
