import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { InputAnswerQuestion } from '../core/models';

@Component({
  selector: 'app-textarea-answer-question',
  templateUrl: 'textarea-answer-question.component.html',
  styleUrls: ['textarea-answer-question.component.scss'],
})

export class TextareaAnswerQuestionComponent implements OnInit{
  @Input() textareaAnsQuestion: InputAnswerQuestion;
  @Input() index: number;
  @Input() formGroup: FormGroup;
  currentAnswer: FormControl;

  ngOnInit() {
    this.currentAnswer = new FormControl();
    this.formGroup.addControl(this.textareaAnsQuestion.id, this.currentAnswer);
  }
}
