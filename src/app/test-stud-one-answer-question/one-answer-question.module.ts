import { NgModule } from '@angular/core';
import { OneAnswerQuestionComponent } from './one-answer-question.component';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from '../material.module';
import { AppRoutingModule } from '../app-routing.module';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    OneAnswerQuestionComponent,
  ],
  exports: [
    OneAnswerQuestionComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule,
    AppRoutingModule,
    ReactiveFormsModule,
  ],
  providers: [],
  bootstrap: [],
})

export class OneAnswerQuestionModule { }
