import { Component, Input, OnInit } from '@angular/core';
import { OneAnswerQuestion } from '../core/models/test';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-one-answer-question',
  templateUrl: './one-answer-question.component.html',
  styleUrls: ['./one-answer-question.component.scss'],
})

export class OneAnswerQuestionComponent implements OnInit{
  @Input() oneAnsQuestion: OneAnswerQuestion;
  @Input() index: number;
  @Input() formGroup: FormGroup;
  answers: FormControl;

  constructor() {}
  ngOnInit() {
    this.answers = new FormControl();
    this.formGroup.addControl(this.oneAnsQuestion.id, this.answers);
  }
}
