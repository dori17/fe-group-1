import { NgModule } from '@angular/core';
import { HeaderComponent  } from './header.component';
import { MaterialModule } from '../material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';

@NgModule({
  declarations: [
    HeaderComponent,
  ],
  exports: [
    HeaderComponent,
  ],
  imports: [
    MaterialModule,
    FlexLayoutModule,
    RouterModule,
    BrowserModule,
  ],
  providers: [],
  bootstrap: [],
})
export class HeaderModule { }
