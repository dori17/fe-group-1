import { Component } from '@angular/core';
import { UserService } from '../core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})

export class HeaderComponent {
  get userName(): string {
    const user = this.userService.getUser();
    return user ? `${user.name} ${user.surname}` : 'Unauthorized';
  }

  get isAuthorized(): boolean {
    console.log(this.userService.isAuthorized());
    return this.userService.isAuthorized();
  }

  titleIcon = 'Logo.png';

  constructor(
    private userService: UserService,
    private router: Router,
  ) {
  }

  onLogout() {
    this.userService.logOut()
      .subscribe(() => this.router.navigate(['logout']));
  }
}
