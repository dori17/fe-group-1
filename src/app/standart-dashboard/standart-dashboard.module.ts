import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { StandartDashboardComponent } from './standart-dashboard.component';
import { MaterialModule } from '../material.module';
import { AppRoutingModule } from '../app-routing.module';
import { PieDiagramModule } from '../pie-diagram/pie-diagram.module';
import { LineDiagramModule } from '../line-diagram/line-diagram.module';
import { TopBarModule } from '../top-bar/top-bar.module';

@NgModule({
  declarations: [
    StandartDashboardComponent,
  ],
  exports: [
    StandartDashboardComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule,
    AppRoutingModule,
    PieDiagramModule,
    LineDiagramModule,
    TopBarModule,
  ],
  providers: [],
  bootstrap: [],
})
export class StandartDashboardModule { }
