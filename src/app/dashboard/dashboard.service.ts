import { Injectable } from '@angular/core';
import { MenuItem } from '../menu-component/menu.component';

@Injectable()
export class DashboardService {
  private menuItemsforStudent: MenuItem[] = [
    { title: 'Тестирование', sub: [// 0
      {
        title: 'Тренировочное',
        link: '/test',
      },
      {
        title:'Итоговое',
        link: '/'},
    ]},
    { title: 'Задачи', sub: [// 1
      {
        title: 'Назначенные',
        link: '/tasks',
      },
      {
        title:'Мои решения',
        link: '/'},
    ]},
    { title: 'Материалы', sub: [// 2
      {
        title: 'Материалы',
        link: '/',
      },
    ]},
  ];

  private menuItemsforTeacher: MenuItem[] = [
    { title: 'Тестирование', sub: [// 0
      {
        title: 'Все',
        link: '/',
      },
      {
        title:'Назначить',
        link: '/'},
    ]},
    { title: 'Задачи', sub: [// 1
      {
        title: 'Все',
        link: '/',
      },
      {
        title:'Назначить',
        link: '/'},
    ]},
    { title: 'Материалы', sub: [// 2
      {
        title: 'Материалы',
        link: './',
      },
    ]},
  ];

  getMenuItemsForStudents() {
    return this.menuItemsforStudent;
  }
  getMenuItemsForTeacher() {
    return this.menuItemsforTeacher;
  }
}
