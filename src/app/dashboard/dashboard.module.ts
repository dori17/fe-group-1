import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { DashboardComponent } from './dashboard.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from '../material.module';
import { TopBarModule } from '../top-bar/top-bar.module';
import { MenuModule } from '../menu-component/menu.module';
import { PieDiagramModule } from '../pie-diagram/pie-diagram.module';
import { LineDiagramModule } from '../line-diagram/line-diagram.module';
import { AppRoutingModule } from '../app-routing.module';
import { StandartDashboardModule } from '../standart-dashboard/standart-dashboard.module';
import { DashboardService } from './dashboard.service';
import { FlexLayoutModule } from '@angular/flex-layout';

@NgModule({
  declarations: [
    DashboardComponent,
  ],
  exports: [
    DashboardComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule,
    TopBarModule,
    MenuModule,
    PieDiagramModule,
    LineDiagramModule,
    AppRoutingModule,
    StandartDashboardModule,
    FlexLayoutModule,
  ],
  providers: [
    DashboardService,
  ],
  bootstrap: [],
})
export class DashboardModule {
}
