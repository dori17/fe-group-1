import { Component, OnInit } from '@angular/core';
import { DashboardService } from './dashboard.service';
import { MenuItem } from '../menu-component/menu.component';

export interface Tile {
  color: string;
  cols: number;
  rows: number;
  text: string;
}

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})

export class DashboardComponent implements OnInit {
  menuItems: MenuItem[];

  constructor(private dashboardService: DashboardService) {
  }

  ngOnInit() {
    this.menuItems = this.dashboardService.getMenuItemsForStudents();
  }
}
