import { NgModule } from '@angular/core';

import { RouterModule, Routes } from '@angular/router';
import { TasksComponent } from '../tasks-component/tasks-component.component';
import { TopBarComponent } from '../top-bar/top-bar.component';
import { TestProcessComponent } from '../test-process/test-process.component';
import { DashboardComponent } from './dashboard.component';
import { DetailTaskComponent } from '../detail-task/detail-task.component';

const routes: Routes = [
  {
    path: '',
    component: DashboardComponent,
    children: [
      { path: 'tasks', component: TasksComponent },
      { path: 'tasks/:id', component: DetailTaskComponent },
      { path: 'test', component: TestProcessComponent },
      { path: 'top-bar', component: TopBarComponent },
      { path: '', redirectTo: 'top-bar', pathMatch: 'full' },
    ],
  },
];

@NgModule({
  exports: [RouterModule],
  imports: [
    RouterModule.forChild(routes),
  ],
})
export class DashboardRoutingModule { }
