import { Component, Input, OnInit } from '@angular/core';
import { UserService } from '../core/services/user.service';

@Component({
  selector: 'app-top-students',
  templateUrl: './top-students.component.html',
  styleUrls: ['./top-students.component.scss'],
})

export class TopStudentsComponent implements OnInit {
  students = [];
  @Input() nameOfTop: string;
  @Input() index: number;
  displayedColumns: string[] = ['surname', 'name', '3', '4'];

  constructor(private userService: UserService) {}

  ngOnInit() {
    this.userService.getStudents()
      .subscribe(students => this.students = students);
  }
}
