import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { TopStudentsComponent } from './top-students.component';
import { MaterialModule } from '../material.module';

@NgModule({
  declarations: [
    TopStudentsComponent,
  ],
  exports: [
    TopStudentsComponent,
  ],
  imports: [
    BrowserModule,
    MaterialModule,
  ],
  providers: [],
  bootstrap: [],
})
export class TopStudentsModule { }
