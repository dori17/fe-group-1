import { Component, Input, OnInit } from '@angular/core';
import { InputAnswerQuestion } from '../core/models';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-input-answer-question',
  templateUrl: 'input-answer-question.component.html',
  styleUrls: ['input-answer-question.component.scss'],
})

export class InputAnswerQuestionComponent implements OnInit {
  @Input() inputAnsQuestion: InputAnswerQuestion;
  @Input() index: number;
  @Input() formGroup: FormGroup;
  currentAnswer: FormControl;

  ngOnInit() {
    this.currentAnswer = new FormControl();
    this.formGroup.addControl(this.inputAnsQuestion.id, this.currentAnswer);
  }
}
