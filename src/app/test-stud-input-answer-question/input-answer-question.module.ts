import { NgModule } from '@angular/core';
import { InputAnswerQuestionComponent } from './input-answer-question.component';
import { BrowserModule } from '@angular/platform-browser';
import { MaterialModule } from '../material.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from '../app-routing.module';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    InputAnswerQuestionComponent,
  ],
  exports: [
    InputAnswerQuestionComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule,
    AppRoutingModule,
    ReactiveFormsModule,
  ],
})

export class InputAnswerQuestionModule { }
