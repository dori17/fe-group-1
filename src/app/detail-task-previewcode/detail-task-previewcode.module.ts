import { NgModule } from '@angular/core';
import { MaterialModule } from '../material.module';
import { PreviewCodeComponent } from './detail-task-previewcode.component';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [
    PreviewCodeComponent,
  ],
  exports: [
    PreviewCodeComponent,
  ],
  imports: [
    MaterialModule,
    CommonModule,
  ],
  providers: [],
  bootstrap: [],
})
export class PreviewCodeModule { }
