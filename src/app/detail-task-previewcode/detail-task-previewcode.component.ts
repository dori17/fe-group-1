import { Component } from '@angular/core';

@Component({
  selector: 'app-preview-code',
  templateUrl: 'detail-task-previewcode.component.html',
  styleUrls: ['detail-task-previewcode.component.scss'],
})

export class PreviewCodeComponent {
  files:string[] = ['#1', '#2', '#3', '#4', '#5'];
}
