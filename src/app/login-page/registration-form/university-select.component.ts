// import { Component, Input } from '@angular/core';
// import {  MatSelectChange } from '@angular/material';

// @Component({
//   selector: 'app-university-select',
//   template: `
//     <div [formGroup]="universitySelect">
//       <mat-form-field>
//         <mat-select
//           required
//           formControlName="university"
//            placeholder="Университет"
//           (selectionChange)="onUniversityChange()"
//         >
//           <mat-option *ngFor="let university of universities" [value]="university.name">
//             {{university.name}}
//           </mat-option>
//         </mat-select>
//       </mat-form-field>
//       <span class="divider"></span>
//       <mat-form-field>
//         <mat-select
//           [ngModel]="currentFaculties"
//           required
//           matInput
//           formControlName="faculty"
//           type="text"
//           placeholder="Факультет"
//         >
//             <mat-option *ngFor="let faculty of currentFaculties" [value]="faculty">
//               {{faculty}}
//             </mat-option>
//         </mat-select>
//       </mat-form-field>
//     </div>
//   `,
//   styleUrls: ['./registration-form.component.scss'],
// })
// export class UniversitySelectComponent {
//   @Input() universities: { name: string, faculties:string[] }[];
//   currentFaculties: string[] = [];

//   constructor() {
//   }

//   onUniversityChange(event: MatSelectChange): void {
//     const university = event.value;
//     console.log(university);
//     const faculties = this.universities
//       .find(obj => obj.name === university)
//       .faculties;
//     this.currentFaculties = faculties;
//   }
// }
