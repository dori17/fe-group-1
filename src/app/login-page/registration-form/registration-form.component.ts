import { Component, OnInit } from '@angular/core';
import { UserService } from '../../core';
import { FormBuilder, Validators } from '@angular/forms';
// import { ApiService } from '../../core/services/api.service';
import { UNIVERSITIES, PRIMARY_SKILLS } from '../../core/mocks/static';
import { of } from 'rxjs';
import { Router } from '@angular/router';

interface University {
  name: string;
  faculties: string[];
}

@Component({
  selector: 'app-registration-form',
  templateUrl: './registration-form.component.html',
  styleUrls: ['./registration-form.component.scss'],
})
export class RegistrationFormComponent implements OnInit {
  universities: University[] = [];
  primarySkills: string[] = [];
  currentFaculties: string[] = [];

  constructor(
    private userService: UserService,
    private fb: FormBuilder,
    private router: Router,
    // private apiService: ApiService
  ) {
  }

  form = this.fb.group({
    email: ['', [Validators.required, Validators.email]],
    name: ['', Validators.required],
    surname: ['', Validators.required],
    role: ['student', Validators.required],
    university: ['', Validators.required],
    faculty: ['', Validators.required],
    graduationYear: [new Date().getFullYear(), Validators.required],
    primarySkill: ['', Validators.required],
  });

  onSubmit() {
    this.userService.register({
      ...this.form.value,
      password: 'Test1234',
    })
      .subscribe(
        user => this.router.navigate(['/login']),
        (err) => {

        });
  }

  onUniversityChange(): void {
    const university = this.form.value['university'];
    const faculties = this.universities
      .find(obj => obj.name === university)
      .faculties;
    this.currentFaculties = faculties;
  }

  ngOnInit() {
    const compareNoCase = (s1: string, s2: string) =>
      s1.toLocaleLowerCase().localeCompare(s2.toLocaleLowerCase());
    // this.apiService.get<University[]>('/universities');
    of(<University[]>UNIVERSITIES)
      .subscribe((universities) => {
        universities.sort((u1, u2) => compareNoCase(u1.name, u2.name));
        universities.forEach(university => university.faculties.sort(compareNoCase));
        this.universities = universities;
      });
    // this.apiService.get<string[]>('/primarySkills')
    of(PRIMARY_SKILLS)
      .subscribe(primarySkills =>
        this.primarySkills = primarySkills.sort(compareNoCase),
      );
  }

}
