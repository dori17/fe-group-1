import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginPageComponent } from './login-page.component';
import { RegistrationFormComponent } from './registration-form/registration-form.component';
import { MaterialModule } from '../material.module';
import { LoginFormComponent } from './login-form/login-form.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router/';
// import { UniversitySelectComponent } from './registration-form/university-select.component';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    FormsModule,
    RouterModule,
    ReactiveFormsModule,
  ],
  declarations: [
    LoginPageComponent,
    LoginFormComponent,
    RegistrationFormComponent,
    // UniversitySelectComponent,
  ],
  exports: [LoginPageComponent],
})
export class LoginPageModule { }
