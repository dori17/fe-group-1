import { Component, OnInit } from '@angular/core';
import { UserService, ApiError } from '../../core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss'],
})
export class LoginFormComponent implements OnInit {
  loginForm = this.fb.group({
    email: ['', [Validators.required, Validators.email]],
    password: ['', Validators.required],
  });
  error: Error;

  constructor(
    private userService: UserService,
    private fb: FormBuilder,
    private router: Router,
  ) {
  }

  ngOnInit() {
  }

  onSubmit() {
    const { email, password } = this.loginForm.value;
    this.userService.logIn(email, password)
      .subscribe(
        (user) => {
          console.log(this.userService.getUser());
          this.router.navigate(['/']);
        },
        (error: ApiError) => {
          console.error('err', error);
          this.loginForm.setErrors({ error: error.description });
        },
    );
  }

}
