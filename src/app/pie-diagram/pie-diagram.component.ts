import { AfterViewInit, Component } from '@angular/core';
import * as Chart from 'chart.js';

@Component({
  selector: 'app-pie-diagram-component',
  templateUrl: './pie-diagram.component.html',
  styleUrls: ['./pie-diagram.component.scss'],
})
export class PieDiagramComponent implements AfterViewInit {
  pieChart: any;
  labels: string[] = ['New', 'InProcess', 'On Hold', 'Accepted', 'Rejected'];
  data: number[] = [150, 200, 300, 200, 700];
  ngAfterViewInit() {
    this.pieChart = new Chart('pieChart', {
      type: 'pie',
      data: {
        labels: this.labels,
        datasets: [{
          label: '#of Votes',
          data: this.data,
          backgroundColor: [
            'rgba(6, 6, 110, 1)',
            'rgba(51, 94, 7, 1)',
            'rgba(245, 231, 34, 1)',
            'rgba(234, 31, 31, 1)',
            'rgba(255, 99, 132, 1)',
          ],
          borderWidth: 1,
        }],
      },
      options: {
        title: {
          text: 'Here is some title',
          display: true,
        },
        legend: {
          position: 'top',
          labels: {
            fontSize: 13,
          },
        },
        responsive: false,
        display: true,
      },
    });
  }
}
