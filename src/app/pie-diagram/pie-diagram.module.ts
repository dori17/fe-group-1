import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { PieDiagramComponent } from './pie-diagram.component';

@NgModule({
  declarations: [
    PieDiagramComponent,
  ],
  exports: [
    PieDiagramComponent,
  ],
  imports: [
    BrowserModule,
  ],
  providers: [],
  bootstrap: [],
})
export class PieDiagramModule { }
