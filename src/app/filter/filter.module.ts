import { NgModule } from '@angular/core';
import { MaterialModule } from '../material.module';
import { FilterComponent } from './filter.component';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [
    FilterComponent,
  ],
  exports: [
    FilterComponent,
  ],
  imports: [
    MaterialModule,
    CommonModule,
  ],
  providers: [],
  bootstrap: [],
})
export class FilterModule { }
