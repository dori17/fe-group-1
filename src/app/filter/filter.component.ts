import { Component, Output, EventEmitter, Input } from '@angular/core';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { MatChipInputEvent } from '@angular/material';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss'],
})

export class FilterComponent{
  @Input() tag: string;
  @Output() nameChange: EventEmitter<string> = new EventEmitter();

  @Output() tagAdded: EventEmitter<string> = new EventEmitter();
  @Output() tagRemoved: EventEmitter<string> = new EventEmitter();
  @Output() markChange: EventEmitter<number> = new EventEmitter();
  @Output() sort: EventEmitter<boolean> = new EventEmitter();

  @Input() filtersMap: any;

  tags: string[] = [];
  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = true;
  readonly separatorKeysCodes: number[] = [ENTER, COMMA];

  items: number[] = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

  constructor() {
  }

  changeName(name: string) {
    this.nameChange.emit(name);
  }
  changeMark(mark: number) {
    this.markChange.emit(mark);
  }
  sorting(flag: boolean) {
    this.sort.emit(flag);
  }
  add(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    if (!value) {
      return;
    }

    if ((value || '').trim()) {
      this.tagAdded.emit(value.trim());
    }
    if (input) {
      input.value = '';
    }
  }

  remove(tag: string): void {
    this.tagRemoved.emit(tag);
  }
}
