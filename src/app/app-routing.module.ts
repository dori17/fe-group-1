import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginPageRoutingModule } from './login-page/login-page-routing.module';
import { DashboardRoutingModule } from './dashboard/dashboard-routing.module';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/top-bar',
    pathMatch: 'full',
  },
  {
    path: 'logout',
    redirectTo: 'login',
  },
];

@NgModule({
  exports: [RouterModule],
  imports: [
    DashboardRoutingModule,
    LoginPageRoutingModule,
    RouterModule.forRoot(routes),
  ],
})
export class AppRoutingModule { }
