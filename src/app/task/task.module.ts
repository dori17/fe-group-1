import { NgModule } from '@angular/core';
import { MaterialModule } from '../material.module';
import { TaskComponent } from './task.component';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [
    TaskComponent,
  ],
  exports: [
    TaskComponent,
  ],
  imports: [
    MaterialModule,
    CommonModule,
  ],
  providers: [],
  bootstrap: [],
})
export class TaskModule { }
