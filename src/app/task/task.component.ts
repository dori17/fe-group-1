import { Component, Input, OnChanges, SimpleChanges, Output, EventEmitter } from '@angular/core';
import { Task } from '../core/models/task';
import { Router } from '@angular/router';

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.scss'],
})

export class TaskComponent implements OnChanges {
  @Input() task:Task;
  @Output() tag: EventEmitter<string> = new EventEmitter();

  assigner:string;

  constructor(
    private router:Router,
  ) {
  }

  ngOnChanges(changes: SimpleChanges) {
    // if (this.task.assignments) {
    //   this.userService.getUserById(changes.task.currentValue.assignments.assignerId)
    //     .subscribe(user => this.assigner = user.surname);
    // }
  }
  onClick(i: number) {
    this.router.navigate(['tasks', i]);
  }
  outputTag(tag: string) {
    this.tag.emit(tag);
  }
  isAssignments() {
    if (this.task.assignments) {
      if (this.task.assignments[0].deadline) {
        return this.task.assignments[0].deadline;
      }
      return 'no deadline';
    }
  }
  isName() {
    if (this.task.assignments) {
      if (this.task.assignments[0].assignerSurname) {
        return this.task.assignments[0].assignerSurname;
      }
      return 'no assigner';
    }
  }
}
