import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TestProcessComponent } from './test-process.component';
import { MaterialModule } from '../material.module';
import { AppRoutingModule } from '../app-routing.module';
import { OneAnswerQuestionModule } from '../test-stud-one-answer-question/one-answer-question.module';
import { MultipleAnswersQuestionModule } from '../test-stud-multiple-answers-question/multiple-answers-question.module';
import { InputAnswerQuestionModule } from '../test-stud-input-answer-question/input-answer-question.module';
import { TextareaAnswerQuestionModule } from '../test-stud-textarea-answer-question/textarea-answer-question.module';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    TestProcessComponent,
  ],
  exports: [
    TestProcessComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule,
    AppRoutingModule,
    OneAnswerQuestionModule,
    MultipleAnswersQuestionModule,
    InputAnswerQuestionModule,
    TextareaAnswerQuestionModule,
    ReactiveFormsModule,
  ],
  providers: [],
  bootstrap: [],
})
export class TestProcessModule { }
