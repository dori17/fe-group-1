import { Component, OnInit } from '@angular/core';
import { TestService } from '../core';
import { AnyQuestion, QuestionType } from '../core/models';
import { FormControl, FormGroup } from '@angular/forms';
import { debounceTime } from 'rxjs/internal/operators';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-test-process-component',
  templateUrl: './test-process.component.html',
  styleUrls: ['./test-process.component.scss'],
})

export class TestProcessComponent implements OnInit {
  questions: AnyQuestion[];
  questionType = QuestionType;
  currentValue: number = 0;
  formGroup: FormGroup;

  formGroupChanges$: BehaviorSubject<object>;

  constructor(private testService: TestService) {
  }

  ngOnInit() {
    this.formGroup = new FormGroup({});
    this.formGroupChanges$ = new BehaviorSubject(this.formGroup.value);

    this.testService.getFullTest()
      .subscribe((questions) => {
        this.questions = questions;
      });

    this.formGroup.valueChanges
      .subscribe((value) => {
        this.updateProgressBar();
        this.formGroupChanges$.next(value);
      });

    this.formGroupChanges$.pipe(
      debounceTime(2500),
    ).subscribe(value => console.log(this.prepareOutput(value)));
  }

  getAnswerForMultipleAnsQuestion(questionValue) {
    return Object.keys(questionValue)
      .filter(answerId => questionValue[answerId]);
  }

  prepareOutput(value) {
    return Object.keys(value).map((questionID) => {
      const question = this.questions.find((question) => {
        return question.id === questionID;
      });
      let answer;
      switch (question.type) {
        case QuestionType.OneAns:
          answer = [value[questionID]];
          break;
        case QuestionType.MultipleAns:
          answer = this.getAnswerForMultipleAnsQuestion(value[questionID] || {});
          break;
        default:
          answer = value[questionID];
      }
      return { questionID, answer };
    });
  }

  checkboxAnswerFilled(id: string): boolean {
    return Object.keys(this.formGroup.controls[id].value).some(
      prop => this.formGroup.controls[id].value[prop]);
  }

  getFilledPercent(): number {
    const allQuestionIds = Object.keys(this.formGroup.value);
    const filledQuestionIds = allQuestionIds.filter(id =>
      ((this.formGroup.controls[id] instanceof FormControl) && this.formGroup.value[id]) ||
      this.formGroup.controls[id] instanceof FormGroup && this.checkboxAnswerFilled(id));
    return (filledQuestionIds.length / allQuestionIds.length) * 100;
  }

  updateProgressBar() {
    this.currentValue = this.getFilledPercent();
  }
}
