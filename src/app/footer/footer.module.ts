import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FooterComponent } from './footer.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from '../material.module';
import { TopBarModule } from '../top-bar/top-bar.module';

@NgModule({
  declarations: [
    FooterComponent,
  ],
  exports: [
    FooterComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule,
    TopBarModule,
  ],
  providers: [],
  bootstrap: [],
})
export class FooterModule { }
