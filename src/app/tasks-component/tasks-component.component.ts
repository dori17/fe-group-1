import { Component, OnInit } from '@angular/core';
import { TaskService, Task, UserService } from '../core';
import { BehaviorSubject } from 'rxjs';
import { debounceTime } from 'rxjs/operators';

@Component({
  selector: 'app-tasks-component',
  templateUrl: './tasks-component.component.html',
  styleUrls: ['./tasks-component.component.scss'],
})

export class TasksComponent implements OnInit{

  filtersMap = {
    taskName: { value: '' },
    tags: { values: [] },
    mark: { value: null },
    sort: { value: false },
  };

  tasks: Task[];
  nameFilter: string = '';
  tagsFilter: string[];
  markFilter: number;
  sort: boolean = false;
  tag:string;

  filterConfig: any = {
  };

  filtersMapChanges$: BehaviorSubject<object> = new BehaviorSubject(this.filtersMap);

  constructor(private taskService: TaskService, private userService: UserService) {
  }

  onClick() {
    console.log(this.userService.getUser());
    this.taskService.getTasks({ userId: this.userService.getUser().id })
    .subscribe((tasks) => {
      console.log(tasks);
    });
  }

  ngOnInit() {
    this.taskService.getTasks({ })
      .subscribe((tasks) => {
        this.tasks = tasks;
      });

    this.filtersMapChanges$.pipe(debounceTime(500)).subscribe(filtersMap => console.log(filtersMap));
  }

  removeTag(tag: string) {
    const index = this.filtersMap.tags.values.indexOf(tag);
    this.filtersMap.tags.values.splice(index, 1);
    this.filtersMapChanges$.next(this.filtersMap);
    this.filterConfig.tags = this.filtersMap.tags.values;
    this.taskService.getTasks(this.filterConfig)
      .subscribe((tasks) => {
        this.tasks = tasks;
      });
    console.log('remove');
  }
  changeTag(tag: string) {
    if (!this.filtersMap.tags.values.includes(tag)) {
      this.filtersMap.tags.values.push(tag);
      this.filtersMapChanges$.next(this.filtersMap);
      this.filterConfig.tags = this.filtersMap.tags.values;
      this.taskService.getTasks(this.filterConfig)
      .subscribe((tasks) => {
        this.tasks = tasks;
      });
      console.log('change');
    }
  }
  changeName(name: string) {
    this.filtersMap.taskName.value = name;
    this.filtersMapChanges$.next(this.filtersMap);
    this.filterConfig.taskName = this.filtersMap.taskName.value;
    console.log(this.filterConfig);
    this.taskService.getTasks(this.filterConfig)
      .subscribe((tasks) => {
        this.tasks = tasks;
      });
  }
  changeMark(mark: number) {
    this.filtersMap.mark.value = mark;
    this.filtersMapChanges$.next(this.filtersMap);
    this.filterConfig.mark = this.filtersMap.mark.value;
    this.taskService.getTasks(this.filterConfig)
      .subscribe((tasks) => {
        this.tasks = tasks;
      });
    // console.log(this.fil);
  }
  sorting(flag: boolean) {
    this.filtersMap.sort.value = flag;
    this.filtersMapChanges$.next(this.filtersMap);
    console.log(this.filtersMap.sort.value);
  }
}
