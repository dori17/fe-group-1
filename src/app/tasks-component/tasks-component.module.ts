import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from '../material.module';
import { AppRoutingModule } from '../app-routing.module';
import { TasksComponent } from './tasks-component.component';
import { TaskModule } from '../task/task.module';
import { FilterModule } from '../filter/filter.module';
@NgModule({
  declarations: [
    TasksComponent,
  ],
  exports: [
    TasksComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule,
    AppRoutingModule,
    TaskModule,
    FilterModule,
  ],
  providers: [],
  bootstrap: [],
})
export class TasksComponentModule { }
