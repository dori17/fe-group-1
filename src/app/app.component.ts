import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
/**
 * @title Tag group with dynamic height based on tab contents
 */

export class AppComponent {

}
