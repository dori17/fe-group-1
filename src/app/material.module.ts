import { NgModule } from '@angular/core';
import { MatTabsModule } from '@angular/material/tabs';
import { MatDividerModule } from '@angular/material/divider';
import { MatListModule } from '@angular/material/list';
import { MatTableModule } from '@angular/material/table';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatButtonModule } from '@angular/material/button';
import {
  MatCheckboxModule, MatGridListModule, MatInputModule, MatProgressBarModule, MatRadioModule,
  MatSidenavModule, MatSelectModule, MatDatepickerModule, MatNativeDateModule,
} from '@angular/material';
import { MatCardModule } from '@angular/material/card';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatBadgeModule } from '@angular/material/badge';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatChipsModule } from '@angular/material/chips';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';

@NgModule({
  imports: [
    MatButtonModule,
    MatTabsModule,
    MatDividerModule,
    MatListModule,
    MatTableModule,
    MatExpansionModule,
    MatGridListModule,
    MatCardModule,
    MatToolbarModule,
    MatBadgeModule,
    MatSidenavModule,
    MatCheckboxModule,
    MatRadioModule,
    MatInputModule,
    MatProgressBarModule,
    MatFormFieldModule,
    MatSelectModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatIconModule,
    MatChipsModule,
    MatTooltipModule,
    MatSlideToggleModule,
  ],
  exports: [
    MatButtonModule,
    MatTabsModule,
    MatDividerModule,
    MatListModule,
    MatTableModule,
    MatExpansionModule,
    MatGridListModule,
    MatCardModule,
    MatToolbarModule,
    MatBadgeModule,
    MatSidenavModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    MatSelectModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatCheckboxModule,
    MatRadioModule,
    MatInputModule,
    MatProgressBarModule,
    MatIconModule,
    MatChipsModule,
    MatTooltipModule,
    MatSelectModule,
    MatSlideToggleModule,
  ],
})
export class MaterialModule {
}
