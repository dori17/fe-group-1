import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material.module';
import { MenuModule } from './menu-component/menu.module';
import { TopStudentsModule } from './top-students/top-students.module';
import { HeaderModule } from './header/header.module';
import { PieDiagramModule } from './pie-diagram/pie-diagram.module';
import { LineDiagramModule } from './line-diagram/line-diagram.module';
import { TopBarModule } from './top-bar/top-bar.module';
import { DashboardModule } from './dashboard/dashboard.module';
import { FooterModule } from './footer/footer.module';
import { AppRoutingModule } from './app-routing.module';
import { TasksComponentModule } from './tasks-component/tasks-component.module';
import { StandartDashboardModule } from './standart-dashboard/standart-dashboard.module';
import { TaskModule } from './task/task.module';
import { CoreModule } from './core/core.module';
import { LoginPageModule } from './login-page/login-page.module';
import { DetailTaskModule } from './detail-task/detail-task.module';
import { TestProcessModule } from './test-process/test-process.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FilterModule } from './filter/filter.module';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { PreviewCodeModule } from './detail-task-previewcode/detail-task-previewcode.module';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule,
    MenuModule,
    PieDiagramModule,
    LineDiagramModule,
    TopBarModule,
    DashboardModule,
    HeaderModule,
    TopStudentsModule,
    FooterModule,
    AppRoutingModule,
    TasksComponentModule,
    StandartDashboardModule,
    TaskModule,
    CoreModule,
    LoginPageModule,
    DetailTaskModule,
    TestProcessModule,
    FlexLayoutModule,
    FilterModule,
    DetailTaskModule,
    AngularFontAwesomeModule,
    PreviewCodeModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {
}
