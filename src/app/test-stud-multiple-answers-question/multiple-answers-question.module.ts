import { NgModule } from '@angular/core';
import { MultipleAnswersQuestionComponent } from './multiple-answers-question.component';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from '../material.module';
import { AppRoutingModule } from '../app-routing.module';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    MultipleAnswersQuestionComponent,
  ],
  exports: [
    MultipleAnswersQuestionComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule,
    AppRoutingModule,
    ReactiveFormsModule,
  ],
})

export class MultipleAnswersQuestionModule { }
