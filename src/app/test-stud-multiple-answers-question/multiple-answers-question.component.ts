import { Component, Input, OnInit } from '@angular/core';
import { MultipleAnswersQuestion } from '../core/models';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-multiple-answers-question',
  templateUrl: './multiple-answers-question.component.html',
  styleUrls: ['./multiple-answers-question.component.scss'],
})

export class MultipleAnswersQuestionComponent implements OnInit {
  @Input() multipleAnsQuestion: MultipleAnswersQuestion;
  @Input() index: number;
  @Input() formGroup: FormGroup;
  newFormGroup: FormGroup;
  formControls: FormControl[] = [];
  currentAnswer: FormControl;

  ngOnInit() {
    this.currentAnswer = new FormControl();
    this.newFormGroup = new FormGroup({});
    this.multipleAnsQuestion.answers.forEach((answer, index) => {
      this.formControls.push(new FormControl());
      this.newFormGroup.addControl(answer.id, this.formControls[index]);
    });
    this.formGroup.addControl(this.multipleAnsQuestion.id, this.newFormGroup);
  }
}
