import { Component, OnInit } from '@angular/core';
import * as Chart from 'chart.js';

@Component({
  selector: 'app-line-diagram-component',
  templateUrl: './line-diagram.component.html',
  styleUrls: ['./line-diagram.component.scss'],
})
export class LineDiagramComponent implements OnInit {
  lineChart: any;
  labels: string[] = ['Average mark', 'Average mark 2'];
  data: number[][] = [[5, 7, 6, 5, 4, 0], [7, 4, 10, 9, 8, 0]];

  ngOnInit() {
    this.lineChart = new Chart('lineChart', {
      type: 'line',
      data: {
        labels: ['Jan', 'Feb', 'March', 'April', 'May', 'June'],
        datasets: [{
          label: this.labels[0],
          data: this.data[0],
          fill: false,
          lineTension: 0.2,
          borderColor: 'red',
          borderWidth: 1,
        },
        {
          label: this.labels[1],
          data: this.data[1],
          fill: true,
          lineTension: 0.2,
          borderColor: 'blue',
          borderWidth: 1,
        }],
      },
      options: {
        scales: {
          yAxes: [{
            display: true,
            ticks: {
              beginAtZero: true,
              steps: 10,
              stepValue: 1,
              min: 0,
              max: 10,
            },
          }],
        },
      },
    });
  }
}
