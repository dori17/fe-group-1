import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { LineDiagramComponent } from './line-diagram.component';

@NgModule({
  declarations: [
    LineDiagramComponent,
  ],
  exports: [
    LineDiagramComponent,
  ],
  imports: [
    BrowserModule,
  ],
  providers: [],
  bootstrap: [],
})
export class LineDiagramModule { }
