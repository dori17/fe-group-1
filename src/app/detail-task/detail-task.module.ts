import { NgModule } from '@angular/core';
import { MaterialModule } from '../material.module';
import { DetailTaskComponent } from './detail-task.component';
import { CommonModule } from '@angular/common';
import { PreviewCodeModule } from '../detail-task-previewcode/detail-task-previewcode.module';

@NgModule({
  declarations: [
    DetailTaskComponent,
  ],
  exports: [
    DetailTaskComponent,
  ],
  imports: [
    MaterialModule,
    CommonModule,
    PreviewCodeModule,
  ],
  providers: [],
  bootstrap: [],
})
export class DetailTaskModule { }
