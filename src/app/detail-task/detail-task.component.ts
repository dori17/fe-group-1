import { Component, OnInit } from '@angular/core';
import { Task, TaskService } from '../core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-detail-task',
  templateUrl: './detail-task.component.html',
  styleUrls: ['detail-task.component.scss'],
})

export class DetailTaskComponent implements OnInit{
  constructor(
    private activeRoute: ActivatedRoute,
    private taskService: TaskService,
  ) {
  }

  mockTags: string[] = ['java', 'hujava', 'back', 'end'];

  id: string;
  task: Task;
  assigner: string;
  isOpened: boolean;

  ngOnInit() {
    this.id = this.activeRoute.snapshot.params['id'];
    this.taskService.getTaskById(this.id)
      .subscribe((task) => {
        this.task = task;
        console.log(task);
        // this.userService.getUserById(this.task.assignments[0].assignerId)
        //   .subscribe(user => this.assigner = user.surname);
      });
  }
  open() {
    this.isOpened = true;
  }
  close() {
    this.isOpened = false;
  }
}
