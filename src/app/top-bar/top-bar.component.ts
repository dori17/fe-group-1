import { Component, OnInit } from '@angular/core';
import { UserService } from '../core/services/user.service';

@Component({
  selector: 'app-top-bar',
  templateUrl: './top-bar.component.html',
  styleUrls: ['./top-bar.component.scss'],
})

export class TopBarComponent implements OnInit{
  students = [];
  labels = [
    'Топ-10 пользователей по итогам прохождения тестов',
    'Топ-10 пользователей по итогам проверки задач',
    'Топ-10 пользователей по рейтингу',
    'Топ-10 самых активных пользователей',
  ];

  constructor(private userService: UserService) {}

  ngOnInit() {
    this.userService.getStudents()
      .subscribe(students => this.students = students);
  }
}
