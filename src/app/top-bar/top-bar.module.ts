import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { TopBarComponent } from './top-bar.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from '../material.module';
import { TopStudentsModule } from '../top-students/top-students.module';
import { FlexLayoutModule } from '@angular/flex-layout';

@NgModule({
  declarations: [
    TopBarComponent,
  ],
  exports: [
    TopBarComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule,
    TopStudentsModule,
    FlexLayoutModule,
  ],
  providers: [],
  bootstrap: [],
})
export class TopBarModule { }
